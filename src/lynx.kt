import java.io.BufferedOutputStream
import java.io.File
import java.io.IOException

fun main(args: Array<String>) {
    val outputStream = BufferedOutputStream(System.out)
    val buffer = ByteArray(65536)

    var fileNames = args.clone()
    if (args.isEmpty()) {
        fileNames = arrayOf("/dev/stdin")
    }

    for (fileName in fileNames) {
        val inputStream = File(fileName).inputStream()
        var readBytes: Int

        while (true) {
            readBytes = try {
                inputStream.read(buffer)
            } catch (e: IOException) {
                break
            }

            if (readBytes < 1) {
                break
            }
            outputStream.write(buffer, 0, readBytes)
            outputStream.flush()
        }
    }
}