#!/bin/bash
dd if=/dev/urandom of=tmp/part1 count=1000
dd if=/dev/urandom of=tmp/part2 count=2000

cat tmp/part1 tmp/part2 > tmp/linked_cat
java -classpath ./out/production/lynx:./lib/kotlin-stdlib.jar:./lib/kotlin-stdlib-jdk7.jar:./lib/kotlin-stdlib-jdk8.jar LynxKt tmp/part1 tmp/part2 > tmp/linked_lynx
md5sum tmp/linked_*


