**Implementation of Unix cat in Kotlin**

***Etymology***

Lynx is a cat that lives in Russia. Kotlin Island is an island near St. Petersburg. Main language of designers 
Kotlin are from Russia.

Lynx also sounds as a program that can link together two files. I'm aware that there's a console browser called lynx.

***Requirements***

Any modern Java runtime should work


**Running**

Precompiled Kotlin binaries and lib dependencies are included for convenience. I'm aware putting binary files in repos 
is ugly.  

**Test**

Test is implemented in shell, depending on standard linux utils and the existing implementation of cat to check if 
files are concatenated correctly. Results are self-explanatory. Tests were implemented like this because of 
implementation speed.


 